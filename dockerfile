FROM ariya/centos7-teamcity-agent-nodejs
MAINTAINER  Jukka Puranen <jukka.puranen@gmail.com>
RUN yum install tar -y
RUN yum install git -y
RUN npm install -g n
RUN n stable
RUN npm install -g bower
RUN mkdir -p /home/teamcity/.ssh
RUN chown -R teamcity:teamcity /home/teamcity/.ssh
VOLUME /home/teamcity/.ssh
